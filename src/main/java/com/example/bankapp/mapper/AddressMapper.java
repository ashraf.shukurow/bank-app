package com.example.bankapp.mapper;

import com.example.bankapp.dto.request.AddressRequest;
import com.example.bankapp.dto.response.AddressResponse;
import com.example.bankapp.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    Address requestToEntity(AddressRequest addressRequest);
    AddressResponse entityToResponse(Address address);
}
