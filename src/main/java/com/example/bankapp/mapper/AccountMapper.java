package com.example.bankapp.mapper;

import com.example.bankapp.dto.request.AccountRequest;
import com.example.bankapp.dto.response.AccountResponse;
import com.example.bankapp.entity.Account;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    Account requestToEntity(AccountRequest accountRequest);
    AccountResponse entityToResponse(Account account);
}
