package com.example.bankapp.mapper;

import com.example.bankapp.dto.request.UserRequest;
import com.example.bankapp.dto.response.UserResponse;
import com.example.bankapp.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User requestToEntity(UserRequest userRequest);
    UserResponse entityToResponse(User user);

}
