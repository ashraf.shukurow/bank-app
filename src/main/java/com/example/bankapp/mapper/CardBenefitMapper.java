package com.example.bankapp.mapper;

import com.example.bankapp.dto.request.CardBenefitRequest;
import com.example.bankapp.dto.response.CardBenefitResponse;
import com.example.bankapp.entity.CardBenefit;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CardBenefitMapper {
    CardBenefit requestToEntity(CardBenefitRequest cardBenefitRequest);
    CardBenefitResponse entityToResponse(CardBenefit cardBenefit);
}
