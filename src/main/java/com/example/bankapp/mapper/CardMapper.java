package com.example.bankapp.mapper;

import com.example.bankapp.dto.request.CardRequest;
import com.example.bankapp.dto.response.CardResponse;
import com.example.bankapp.entity.Card;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CardMapper {
    Card requestToEntity(CardRequest cardRequest);
    CardResponse entityToResponse(Card card);
}
