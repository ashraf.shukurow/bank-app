package com.example.bankapp.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "accounts")
@Data
@NamedEntityGraph(name = "account-with-user-cards-benefits", attributeNodes = {
        @NamedAttributeNode(value = "user"),
        @NamedAttributeNode(value = "cards", subgraph = "cards.benefits")
}, subgraphs = {
        @NamedSubgraph(name = "cards.benefits", attributeNodes = {
                @NamedAttributeNode(value = "benefits")
        })

})
//@NamedQuery(name = "test",query = "select a from Account a")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    private String accountNumber;
    private double balance;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Card> cards;
}