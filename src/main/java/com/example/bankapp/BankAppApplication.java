package com.example.bankapp;

import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.repository.UserRepository;
import com.example.bankapp.service.StudentService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class BankAppApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(BankAppApplication.class, args);
    }

    private final UserRepository userRepository;
    private final AccountRepository accountRepository;
    private final StudentService studentService;
    private final StudentRepository studentRepository;


    @Override
    public void run(String... args) throws Exception {
//        System.out.println(userRepository.findAll());
//        System.out.println(accountRepository.findAll());
//        System.out.println(accountRepository.getAccountByCustom());
//        accountRepository.findByCustomGraph().forEach(System.out::println);
//        accountRepository.findByAccountNumber("account1").forEach(System.out::println);
//        accountRepository.findAllCustom().forEach(System.out::println);
//        accountRepository.findByA().forEach(System.out::println);
//        Student ashraf = Student.builder().
//                name("ashraf")
//                .age(20)
//                .username("shukurlu")
//                .gender("M").build();
//        Student qulu = Student.builder().
//                name("qulu")
//                .age(20)
//                .username("bedelov")
//                .gender("M").build();
//        List<Student> students = List.of(ashraf, qulu);
//        for (Student s : students) {
//            studentService.saveStudent(s);
//        }
//        studentService.getAllStudent("ashraf","bedelov" ).forEach(System.out::println);
studentRepository.getAllStudents("a").forEach(System.out::println);

    }
}
