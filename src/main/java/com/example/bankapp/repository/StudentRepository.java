package com.example.bankapp.repository;

import com.example.bankapp.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student,Long>, JpaSpecificationExecutor<Student> {
//    Optional<Student> findByNameOrAgeOrGenderOrUsername(String name,String gender,String username,int age);

    @Query("select s from Student s where (:name is null or s.name like :name)")
    List<Student> getAllStudents(@Param("name") String name);
}
