package com.example.bankapp.repository;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    @Query("select a from Account a join fetch a.user u join fetch a.cards c join fetch c.benefits b")
    Optional<List<Account>> getAccountByCustom();

    @Query(value = "select * from accounts a " +
            "join users u on u.id=a.user_id" +
            " join cards c on a.id=c.account_id " +
            "join card_benefits b on c.id=b.card_id", nativeQuery = true)
    List<Account> finByCustomNative();

    @EntityGraph(attributePaths = {"user", "cards", "cards.benefits"})
    List<Account> findByAccountNumber(String accountNo);

    @Query("select new com.example.bankapp.dto.AccountDto(a.id,a.accountNumber,a.balance," +
            "u.username,u.password," +
            "c.cardNumber,c.cardType,c.expirationDate) from Account a join  a.user u join  a.cards c join  c.benefits b")
    List<AccountDto> findAllCustom();

//    @EntityGraph(value = "account-with-user-cards-benefits",type = EntityGraph.EntityGraphType.FETCH)
//    List<Account> finByAccountNumber();


//    @Query(name = "test")
//    List<Account> findByA();
       boolean existsByAccountNumber(String accountNumber);

}
