package com.example.bankapp.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlreadyExistsException extends RuntimeException{
    private final HttpStatus httpStatus=HttpStatus.CONFLICT;
    private  String message;


}
