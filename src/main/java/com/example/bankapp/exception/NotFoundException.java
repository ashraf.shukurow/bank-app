package com.example.bankapp.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class NotFoundException extends RuntimeException {
    private final HttpStatus httpStatus=HttpStatus.NOT_FOUND;
    private final String message;
}
