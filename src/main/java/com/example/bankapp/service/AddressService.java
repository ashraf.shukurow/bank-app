package com.example.bankapp.service;

import com.example.bankapp.dto.request.AddressRequest;
import com.example.bankapp.dto.request.CardRequest;
import com.example.bankapp.dto.response.AddressResponse;
import com.example.bankapp.dto.response.CardResponse;

import java.util.List;

public interface AddressService {
    void createAddress(AddressRequest addressRequest);
    AddressResponse getAddressById(Long id);
    List<AddressResponse> getAllAddress();
    void updateAddress(AddressRequest addressRequest,Long id);
}
