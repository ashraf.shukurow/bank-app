package com.example.bankapp.service;

import com.example.bankapp.dto.request.UserRequest;
import com.example.bankapp.dto.response.UserResponse;

import java.util.List;

public interface UserService {

    UserResponse getUserById(Long id);
    List<UserResponse>  getAllUser();
    void createUser(UserRequest userRequest);
    void updateUser(UserRequest userRequest,Long id);

}
