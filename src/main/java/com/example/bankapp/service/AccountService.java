package com.example.bankapp.service;

import com.example.bankapp.dto.request.AccountRequest;
import com.example.bankapp.dto.response.AccountResponse;

import java.util.List;

public interface AccountService {
    AccountResponse getAccountById(Long id);
    void createAccount(AccountRequest accountRequest);
    List<AccountResponse> getAllAccount();
    void updateAccount(AccountRequest accountRequest,Long id);


}
