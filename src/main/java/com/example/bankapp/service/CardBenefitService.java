package com.example.bankapp.service;

import com.example.bankapp.dto.request.CardBenefitRequest;
import com.example.bankapp.dto.response.CardBenefitResponse;

import java.util.List;

public interface CardBenefitService {
    void createCardBenefit(CardBenefitRequest cardBenefitRequest);
    CardBenefitResponse getCardBenefitById(Long id);
    List<CardBenefitResponse> getAllCardBenefit();
    void updateCardBenefit(Long id, CardBenefitRequest cardBenefitRequest);
}