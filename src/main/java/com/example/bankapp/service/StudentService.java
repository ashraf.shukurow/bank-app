package com.example.bankapp.service;

import com.example.bankapp.entity.Student;

import java.util.List;

public interface StudentService {
    Long saveStudent(Student student);

    List<Student> getAllStudent(String name,String surname);
}
