package com.example.bankapp.service.impl;

import com.example.bankapp.dto.request.UserRequest;
import com.example.bankapp.dto.response.UserResponse;
import com.example.bankapp.entity.User;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.UserMapper;
import com.example.bankapp.repository.UserRepository;
import com.example.bankapp.service.UserService;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

//    @Override
//    public User getUser(Long id) {
//
//        User user = null;
//        Optional<User> userOptional = userRepository.findById(id);
//        if (userOptional.isPresent()) {
//            user = userOptional.get();
//        }
//        return user;
//    }

    @Override
    public UserResponse getUserById(Long id) {
        User user=userRepository.findById(id).orElseThrow(()->new NotFoundException("user not found with this id: "+id));
        return userMapper.entityToResponse(user);
    }

    @Override
    public List<UserResponse> getAllUser() {
        List<User> users=userRepository.findAll();
        return users.stream().map(userMapper::entityToResponse).toList();
    }

    @Override
    public void createUser(UserRequest userRequest) {
         userRepository.save(userMapper.requestToEntity(userRequest));
    }

    @Override
    public void updateUser(UserRequest userRequest, Long id) {

    }
}
