package com.example.bankapp.service.impl;

import com.example.bankapp.dto.request.AccountRequest;
import com.example.bankapp.dto.response.AccountResponse;
import com.example.bankapp.entity.Account;
import com.example.bankapp.exception.AlreadyExistsException;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.AccountMapper;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    private final AccountMapper accountMapper;

    @Override
    public AccountResponse getAccountById(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no account with this id: " + id));
        return accountMapper.entityToResponse(account);
    }

    @Override
    public void createAccount(AccountRequest accountRequest) {
        Account account=accountMapper.requestToEntity(accountRequest);
        if (accountRepository.existsByAccountNumber(account.getAccountNumber())){
            throw new AlreadyExistsException("There is already account with this number: "+account.getAccountNumber());
        }
        accountRepository.save(account);
    }

    @Override
    public List<AccountResponse> getAllAccount() {
       List<Account> accounts=accountRepository.findAll();
       return accounts.stream().map(accountMapper::entityToResponse).toList();
    }

    @Override
    public void updateAccount(AccountRequest accountRequest, Long id) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no account with this id: " + id));
        Account updatedAccount=accountMapper.requestToEntity(accountRequest);
        updatedAccount.setId(account.getId());
        accountRepository.save(updatedAccount);

    }
}
