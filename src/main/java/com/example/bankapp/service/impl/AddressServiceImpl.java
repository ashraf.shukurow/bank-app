package com.example.bankapp.service.impl;

import com.example.bankapp.dto.request.AddressRequest;
import com.example.bankapp.dto.response.AddressResponse;
import com.example.bankapp.entity.Address;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.AddressMapper;
import com.example.bankapp.repository.AddressRepository;
import com.example.bankapp.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;
    @Override
    public void createAddress(AddressRequest addressRequest) {
       addressRepository.save(addressMapper.requestToEntity(addressRequest));
    }

    @Override
    public AddressResponse getAddressById(Long id) {
        return addressMapper.entityToResponse(addressRepository.findById(id).orElseThrow(()->new NotFoundException("there is no address with id: "+id)));
    }

    @Override
    public List<AddressResponse> getAllAddress() {
          return addressRepository
                  .findAll()
                  .stream()
                  .map(addressMapper::entityToResponse)
                  .toList();
    }

    @Override
    public void updateAddress(AddressRequest addressRequest, Long id) {
         Address address=addressRepository.findById(id).orElseThrow(()->new NotFoundException("there is no address with id: "+id));
         Address updatedAddress=addressMapper.requestToEntity(addressRequest);
         updatedAddress.setId(address.getId());
         addressRepository.save(updatedAddress);
    }
}
