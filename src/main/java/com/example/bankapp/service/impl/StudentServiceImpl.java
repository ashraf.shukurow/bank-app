package com.example.bankapp.service.impl;

import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.service.StudentService;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    @Override
    public Long saveStudent(Student student) {
        return studentRepository.save(student).getId();

    }

    @Override
    public List<Student> getAllStudent(String name, String surname) {
        Specification<Student> studentSpecification1=null;
        List<Predicate> predicates=new ArrayList<>();
        //root base entity,creteria operations
            studentSpecification1 = (root, cq, cb) ->{
               if(name!=null){
                   predicates.add(cb.equal(root.get("name"), name));
               }
               if(surname!=null){
                   predicates.add(cb.equal(root.get("username"), surname));
               }
                cq.where(cb.or(predicates.toArray(new Predicate[0])));
               return cq.getRestriction();
            };
          return   studentRepository.findAll(studentSpecification1);

//        Specification<Student> studentSpecification = new Specification<Student>() {
//            @Override
//            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
////              return   criteriaBuilder.equal(root.get("name"),name);
//                return criteriaBuilder.like(root.get("name"), "%" + name + "%");
//
//            }
//        };
//        return studentRepository.findAll(studentSpecification1);
    }
}
