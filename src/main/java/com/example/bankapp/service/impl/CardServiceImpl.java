package com.example.bankapp.service.impl;

import com.example.bankapp.dto.request.CardRequest;
import com.example.bankapp.dto.response.CardResponse;
import com.example.bankapp.entity.Account;
import com.example.bankapp.entity.Card;
import com.example.bankapp.exception.AlreadyExistsException;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.CardMapper;
import com.example.bankapp.repository.CardRepository;
import com.example.bankapp.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {
    private final CardRepository cardRepository;
    private final CardMapper cardMapper;

    @Override
    public void createCard(CardRequest cardRequest) {
        Card card = cardMapper.requestToEntity(cardRequest);
        if (cardRepository.existsByCardNumber(card.getCardNumber())) {
            throw new AlreadyExistsException("there is already card with this number: " + card.getCardNumber());
        }
        cardRepository.save(card);
    }

    @Override
    public CardResponse getCardById(Long id) {
       Card card=cardRepository.findById(id).orElseThrow(()->new NotFoundException("there is no card with this id: "+id));
       return cardMapper.entityToResponse(card);
    }

    @Override
    public List<CardResponse> getAllCard() {
       List<Card> cards=cardRepository.findAll();
       return cards.stream().map(cardMapper::entityToResponse).toList();
    }

    @Override
    public void updateCard(CardRequest cardRequest, Long id) {

    }
}
