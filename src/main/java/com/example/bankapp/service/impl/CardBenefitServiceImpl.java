package com.example.bankapp.service.impl;

import com.example.bankapp.dto.request.CardBenefitRequest;
import com.example.bankapp.dto.response.CardBenefitResponse;
import com.example.bankapp.dto.response.CardResponse;
import com.example.bankapp.entity.CardBenefit;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.CardBenefitMapper;
import com.example.bankapp.repository.CardBenefitRepository;
import com.example.bankapp.service.CardBenefitService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CardBenefitServiceImpl implements CardBenefitService {
    private final CardBenefitRepository cardBenefitRepository;
    private final CardBenefitMapper cardBenefitMapper;

    @Override
    public void createCardBenefit(CardBenefitRequest cardBenefitRequest) {
        cardBenefitRepository.save(cardBenefitMapper.requestToEntity(cardBenefitRequest));
    }

    @Override
    public CardBenefitResponse getCardBenefitById(Long id) {
        CardBenefit cardBenefit = cardBenefitRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no card benefit with id: " + id));
        return cardBenefitMapper.entityToResponse(cardBenefit);
    }

    @Override
    public List<CardBenefitResponse> getAllCardBenefit() {
        return cardBenefitRepository
                .findAll()
                .stream()
                .map(cardBenefitMapper::entityToResponse)
                .toList();
    }

    @Override
    public void updateCardBenefit(Long id, CardBenefitRequest cardBenefitRequest) {
        CardBenefit cardBenefit = cardBenefitRepository.findById(id).orElseThrow(() -> new NotFoundException("there is no card benefit with id: " + id));
        CardBenefit updatedCardBenefit = cardBenefitMapper.requestToEntity(cardBenefitRequest);
        updatedCardBenefit.setId(cardBenefit.getId());
        cardBenefitRepository.save(updatedCardBenefit);
    }
}
