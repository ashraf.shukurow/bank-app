package com.example.bankapp.service;

import com.example.bankapp.dto.request.CardRequest;
import com.example.bankapp.dto.response.CardResponse;

import java.util.List;

public interface CardService {
    void createCard(CardRequest cardRequest);
    CardResponse getCardById(Long id);
    List<CardResponse> getAllCard();
    void updateCard(CardRequest cardRequest,Long id);

}
