package com.example.bankapp.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class AccountDto {
    private Long id;
    private String accountNumber;
    private double balance;
    private String username;
    private String password;
    private String cardNumber;
    private String cardType;
    private String expirationDate;
}
