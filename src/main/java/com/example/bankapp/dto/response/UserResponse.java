package com.example.bankapp.dto.response;

import lombok.Data;

@Data
public class UserResponse {
    private String username;

    private String password;
}
