package com.example.bankapp.dto.request;

import lombok.Data;

@Data
public class AddressRequest {
    private String street;
    private String city;
    private String postalCode;
    private Long userId;
}
