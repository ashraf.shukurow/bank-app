package com.example.bankapp.dto.request;

import lombok.Data;

@Data
public class AccountRequest {
    private String accountNumber;
    private Long userId;
    private double balance;
}
