package com.example.bankapp.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CardRequest {
    private String cardNumber;
    private String cardType;
    private String expirationDate;
    private Long accountId;
}
