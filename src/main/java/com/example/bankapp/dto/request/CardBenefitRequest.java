package com.example.bankapp.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardBenefitRequest {
    private String name;
    private String description;
    private Long userId;
}
