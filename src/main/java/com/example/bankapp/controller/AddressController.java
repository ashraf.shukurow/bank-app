package com.example.bankapp.controller;


import com.example.bankapp.dto.request.AddressRequest;
import com.example.bankapp.dto.response.AddressResponse;
import com.example.bankapp.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/address/")
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/")
    public void createAddress(@RequestBody AddressRequest addressRequest){
        addressService.createAddress(addressRequest);
    }
    @GetMapping("/{id}")
    public ResponseEntity<AddressResponse> getAddressById(@PathVariable Long id){
        return ResponseEntity.ok(addressService.getAddressById(id));
    }
    @GetMapping("/")
    public ResponseEntity<List<AddressResponse>> getAllAddress(){
        return ResponseEntity.ok(addressService.getAllAddress());
    }

    @PutMapping("/{id}")
    public void updateAddress(@RequestBody AddressRequest addressRequest,@PathVariable Long id){
        addressService.updateAddress(addressRequest,id);
    }
}
