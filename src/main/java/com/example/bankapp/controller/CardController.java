package com.example.bankapp.controller;

import com.example.bankapp.dto.request.CardRequest;
import com.example.bankapp.dto.response.CardResponse;
import com.example.bankapp.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/cards/")
public class CardController {

    private final CardService cardService;

    @PostMapping("/")
    public void createCard(@RequestBody CardRequest cardRequest){
        cardService.createCard(cardRequest);
    }
    @GetMapping("/{id}")
    public ResponseEntity<CardResponse> getCardById(@PathVariable Long id){
        return ResponseEntity.ok(cardService.getCardById(id));
    }
    @GetMapping("/")
    public ResponseEntity<List<CardResponse>> getAllCard(){
        return ResponseEntity.ok(cardService.getAllCard());
    }
    @PutMapping("/{id}")
    public void updateCard(@RequestBody CardRequest cardRequest,@PathVariable Long id){
        cardService.updateCard(cardRequest,id);

    }

}
