package com.example.bankapp.controller;

import com.example.bankapp.dto.request.CardBenefitRequest;
import com.example.bankapp.dto.response.CardBenefitResponse;
import com.example.bankapp.service.CardBenefitService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/cardBenefit/")
@RequiredArgsConstructor
public class CardBenefitController {
    private final CardBenefitService cardBenefitService;

    @PostMapping("/")
    public void createCardBenefit(@RequestBody CardBenefitRequest cardBenefitRequest) {
        cardBenefitService.createCardBenefit(cardBenefitRequest);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CardBenefitResponse> getCardBenefitById(@PathVariable Long id) {
        return ResponseEntity.ok(cardBenefitService.getCardBenefitById(id));
    }

    @GetMapping("/")
    public ResponseEntity<List<CardBenefitResponse>> getAllCardBenefit(){
        return ResponseEntity.ok(cardBenefitService.getAllCardBenefit());
    }

    @PutMapping("/{id}")
    public void updateCardBenefits(@PathVariable Long id,@RequestBody CardBenefitRequest cardBenefitRequest){
        cardBenefitService.updateCardBenefit(id,cardBenefitRequest);
    }
}
