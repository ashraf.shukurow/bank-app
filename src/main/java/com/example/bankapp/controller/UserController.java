package com.example.bankapp.controller;

import com.example.bankapp.dto.request.UserRequest;
import com.example.bankapp.dto.response.UserResponse;
import com.example.bankapp.entity.User;
import com.example.bankapp.service.UserService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user/")
public class UserController {

    private final UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }
    @PostMapping("/")
    public void createUser(@RequestBody UserRequest userRequest){
        userService.createUser(userRequest);
    }
    @GetMapping("/")
    public ResponseEntity<List<UserResponse>> getAllUser(){
        return ResponseEntity.ok(userService.getAllUser());
    }
    @PutMapping("/{id}")
    public void updateUser(@RequestBody UserRequest userRequest,@PathVariable Long id){
        userService.updateUser(userRequest,id);

    }

}
