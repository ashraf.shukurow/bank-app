package com.example.bankapp.controller;

import com.example.bankapp.dto.request.AccountRequest;
import com.example.bankapp.dto.response.AccountResponse;
import com.example.bankapp.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/account/")
public class AccountController {
    private final AccountService accountService;

    @PostMapping("/")
    public void createAccount(@RequestBody AccountRequest accountRequest) {
        accountService.createAccount(accountRequest);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountResponse> getAccountById(@PathVariable Long id) {
        return ResponseEntity.ok(accountService.getAccountById(id));
    }

    @GetMapping("/")
    public ResponseEntity<List<AccountResponse>> getAlAccount() {
        return ResponseEntity.ok(accountService.getAllAccount());
    }

    @PutMapping("/{id}")
    public void updateAccount(@RequestBody AccountRequest accountRequest,@PathVariable Long id){
        accountService.updateAccount(accountRequest,id);

    }
}
